#CONFIG += console
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vk_useragent
TEMPLATE = app

LIBS += -lcurl -lboost_regex
QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp \
    cpagedownloader.cpp \
    cpageparser.cpp \
    cposts.cpp \
    cauth.cpp \
    cfriends.cpp \
    cuserinfo.cpp \
    cpoststhread.cpp \
    cuserinfothread.cpp \
    cfriendsthread.cpp \
    cuserinfowidget.cpp \
    cfriendswidget.cpp \
    cpostswidget.cpp \
    mainwindow.cpp

HEADERS += \
    cpagedownloader.h \
    cpageparser.h \
    cposts.h \
    vk.h \
    cauth.h \
    cfriends.h \
    cuserinfo.h \
    thread.h \
    cpoststhread.h \
    cuserinfothread.h \
    cfriendsthread.h \
    mainwindow.h \
    cuserinfowidget.h \
    cfriendswidget.h \
    cpostswidget.h

FORMS += \
    mainwindow.ui

