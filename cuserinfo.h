#ifndef CUSERINFO_H
#define CUSERINFO_H
#include "vk.h"
#include "cpageparser.h"
#include <list>
#include <string>

namespace vk{
    class cUserInfo : public cPageParser, public std::list<user_info_t>{
    public:
        cUserInfo(const std::string& user_id);
        cUserInfo(const std::string& user_id, const std::string& user, const std::string& password);
    protected:
        virtual void process(match_t &what);
    private:
        void setUserId(const std::string& user_id);
    };
}

#endif // CUSERINFO_H
