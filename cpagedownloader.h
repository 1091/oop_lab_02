#ifndef CPAGEDOWNLOADER_H
#define CPAGEDOWNLOADER_H
#include <string>

namespace vk{
    class cPageDownloader{
        std::string m_page;
    public:
        cPageDownloader(const std::string& page, const std::string &cookies);
        const std::string& page() const;
        operator std::string () const { return page(); }
    };
}

#endif // CPAGEDOWNLOADER_H
