#include "cfriends.h"

namespace vk{
    cFriends::cFriends(const std::string &user, const std::string &password)
        : cPageParser(user, password){
        setRegex("<div class=\"friends_field\"><a href=\"/([^\"]+?)\"><b>(.+?)</b></a></div>");
        setPage("vk.com/friends");
    }
    void cFriends::process(match_t &what){
        author_t author = {convert(what[1]), convert(what[2])};
        push_back(author);
    }
}
