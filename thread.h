#ifndef THREAD_H
#define THREAD_H
#include <pthread.h>

template <class T>
void* thread_body(void* data){
    T* functor = reinterpret_cast<T*>(data);
    (*functor)();
    return 0;
}

template<class FunctorType>
class Thread{
public:
    Thread(FunctorType &f) : functor(f) {}
    void start(){
        void* data = reinterpret_cast<void*>(&functor);
        if (pthread_create(&thread, (pthread_attr_t *)NULL, thread_body<FunctorType>, data) != 0){
            throw std::runtime_error("An error in pthread_create occured");
        }
    }
    void wait(){
        if(pthread_join(thread, NULL) != 0){
            throw std::runtime_error("An error in pthread_join occured");
        }
    }
private:
    FunctorType functor;
    pthread_t thread;
};

#endif //THREAD_H

