#include "cfriendswidget.h"

cFriendsWidget::cFriendsWidget(QWidget *parent, QLineEdit* loginWidget, QLineEdit* passwordWidget) :
    QTableWidget(parent){
    this->loginWidget = loginWidget;
    this->passwordWidget = passwordWidget;
    verticalHeader()->setVisible(false);
    horizontalHeader()->setVisible(false);
    verticalScrollBar()->setVisible(false);
    setShowGrid(false);
    setSelectionMode(QTableWidget::NoSelection);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    horizontalHeader()->setStretchLastSection(true);
}

void cFriendsWidget::update(){
    friendsThread = new vk::cFriendsThread(loginWidget->text().toStdString(), passwordWidget->text().toStdString());
    connect(friendsThread, SIGNAL(friendsIsGet(bool)), this, SLOT(getNewData(bool)));
    friendsThread->getFriends();
}

void cFriendsWidget::getNewData(bool succes){
    if(succes){
        setRowCount(15);
        setColumnCount(1);
        int k = 0;
        for (auto it : (*friendsThread)){
            QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(QString::fromUtf8(it.name.c_str())));
            setItem(k, 0, newItem);
            resizeRowsToContents();
            ++k;
        }
    } else {
        setRowCount(1);
        setColumnCount(1);
        clear();
        QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg("Wrong LOGIN or PASSWORD"));
        setItem(0, 0, newItem);
        resizeRowsToContents();
    }
}

