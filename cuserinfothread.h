#ifndef CUSERINFOTHREAD_H
#define CUSERINFOTHREAD_H

#include "cuserinfo.h"
#include "thread.h"
#include <mutex>
#include <QObject>

namespace vk{

    class cUserInfoThread : public QObject, public cUserInfo{
    Q_OBJECT
    public:
        cUserInfoThread(const std::string &id);
        void getUserInfo();
        void getThreadSafety();
     private:
        std::string id;
        static std::mutex mGetMutex;
     signals:
         bool infoIsGet(bool);
    };

    class fFunctorUserInfo{
    public:
        fFunctorUserInfo(cUserInfoThread* infoThread);
        void operator () ();
    private:
        cUserInfoThread* infoThread;
    };
}
#endif // CUSERINFOTHREAD_H
