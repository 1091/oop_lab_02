#ifndef CPOSTSWIDGET_H
#define CPOSTSWIDGET_H

#include "cpoststhread.h"
#include <QWidget>
#include <QTableWidget>
#include <QHeaderView>
#include <QScrollBar>
#include <QObject>
#include <QLineEdit>

class cPostsWidget : public QTableWidget{
    Q_OBJECT
public:
    explicit cPostsWidget(QWidget *parent = 0, QLineEdit* loginWidget = 0, QLineEdit* passwordWidget = 0);
private:
    vk::cPostsThread *postsThread;
    QLineEdit* loginWidget;
    QLineEdit* passwordWidget;
public slots:
    void update();
    void getNewData(bool);
};

#endif // CPOSTSWIDGET_H
