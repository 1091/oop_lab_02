#include "cfriendsthread.h"

namespace vk{
    cFriendsThread::cFriendsThread(const std::string &login, const std::string &password) : cFriends(login, password) {}
    std::mutex cFriendsThread::mGetMutex;

    void cFriendsThread::getThreadSafety(){
        mGetMutex.lock();
        bool success = get();
        mGetMutex.unlock();
        emit friendsIsGet(success);
    }

    cFunctorFriends::cFunctorFriends(cFriendsThread* friendsThread){
        this->friendsThread = friendsThread;
    }

    void cFriendsThread::getFriends(){
        cFunctorFriends functor(this);
        Thread<vk::cFunctorFriends>thread(functor);
        thread.start();
        thread.wait();
    }

    int cFunctorFriends::operator () (){
        friendsThread->getThreadSafety();
        return 0;
    }
}

