#ifndef CFRIENDS_H
#define CFRIENDS_H
#include "vk.h"
#include "cpageparser.h"
#include <list>
#include <string>

namespace vk{
    class cFriends : public cPageParser, public std::list<author_t>{
    public:
        cFriends(const std::string &user, const std::string &password);
    protected:
        virtual void process(match_t &what);
    };
}

#endif // CFRIENDS_H
