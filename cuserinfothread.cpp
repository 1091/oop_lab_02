#ifndef CUSERINFOTHREAD_CPP
#define CUSERINFOTHREAD_CPP

#include "cuserinfothread.h"

namespace vk{

    std::mutex cUserInfoThread::mGetMutex;
    cUserInfoThread::cUserInfoThread(const std::string &id) : cUserInfo(id) {}

    void cUserInfoThread::getUserInfo(){
        fFunctorUserInfo functor(this);
        Thread<fFunctorUserInfo> thread(functor);
        thread.start();
        thread.wait();
    }

    void cUserInfoThread::getThreadSafety(){
        mGetMutex.lock();
        bool success = get();
        mGetMutex.unlock();
        emit infoIsGet(success);
    }

    fFunctorUserInfo::fFunctorUserInfo(cUserInfoThread* infoThread){
        this->infoThread = infoThread;
    }

    void fFunctorUserInfo::operator () (){
        infoThread->getThreadSafety();
    }
}

#endif // CUSERINFOTHREAD_CPP
