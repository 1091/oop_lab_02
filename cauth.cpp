#include "cauth.h"
#include <iostream>
#include <sstream>
#include <curl/curl.h>

namespace vk{    
    cAuth::cAuth(){
    }
    cAuth::cAuth(const std::string &user, const std::string &password)
         throw (std::runtime_error){
        struct local{
            static size_t write_data(void* buffer, size_t size, size_t nmemb, void* userp){
                std::ostream& stream = *reinterpret_cast<std::ostream*>(userp);
                stream.write(reinterpret_cast<char*>(buffer), size * nmemb);
                return size * nmemb;
            };
        };
        std::stringstream stream;
        CURL* curl = curl_easy_init();
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, local::write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA,     &stream);
        curl_easy_setopt(curl, CURLOPT_HEADER,        1L);
            std::ostringstream postfields;
        postfields << "role=al_frame&ip_h=6157e2cb00200d24e7&pass=" << password << "&email=" << user;
            curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS,    postfields.str().c_str());
        curl_easy_setopt(curl, CURLOPT_URL,     "login.vk.com/?act=login");
        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        curl_easy_perform(curl);
        stream << '\0';

        std::string s;
        std::ostringstream cookies;
        unsigned count = 0;

        while(!stream.eof())
        {	std::getline(stream, s);
            if(s.find("Set-Cookie: ") == 0)
            {	if(count++)
                    cookies << ";";

                cookies << s.substr(12, s.find(";") - 12);
            };
        };
        curl_easy_cleanup(curl);
        m_cookies = cookies.str();
        if (m_cookies.length() == 0)
            throw std::runtime_error("Failed to get cookies from vk.com");
    }
    const std::string &cAuth::cookies() const{
        return m_cookies;
    }
}
