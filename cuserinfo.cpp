#include "cuserinfo.h"

namespace vk{
    cUserInfo::cUserInfo(const std::string &user_id){
        setUserId(user_id);
    }
    cUserInfo::cUserInfo(const std::string &user_id,
                             const std::string &user,
                             const std::string &password)
        : cPageParser(user, password){
        setUserId(user_id);
    }
    void cUserInfo::process(cPageParser::match_t &what){
        user_info_t info;
        info.first  = escape_html(convert(what[1]));
        info.second = escape_html(convert(what[2]));
        push_back( info );
    }
    void cUserInfo::setUserId(const std::string& user_id){
        setRegex("<div class=\"label fl_l\">(.+?)</div>.*?"
                               "<div class=\"labeled fl_l\">(.+?)</div>");
        setPage("vk.com/" + user_id);
    }
}
