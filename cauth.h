#ifndef CVKAUTH_H
#define CVKAUTH_H
#include <string>
#include <stdexcept>

namespace vk{
    class cAuth{
        std::string m_cookies;
    public:
        cAuth();
        cAuth(const std::string& user, const std::string& password) throw (std::runtime_error);
        const std::string& cookies() const;
        operator std::string () const { return cookies(); }
    };

}

#endif // CVKAUTH_H
