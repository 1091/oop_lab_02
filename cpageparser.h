#ifndef CPAGEPARSER_H
#define CPAGEPARSER_H
#include <boost/regex.hpp>
#include <iconv.h>
#include "cauth.h"

namespace vk{
    class cPageParser{
    public:
        cPageParser();
        cPageParser(const std::string& user, const std::string& password);
        virtual ~cPageParser();
        bool get();
    protected:
        //bool get();
        typedef boost::match_results<std::string::iterator> match_t;
        void setPage(const std::string &page);
        void setRegex(const std::string &regex);
        virtual void process(match_t &what) = 0;
        std::string convert(const std::string &source);
        std::string escape_html(const std::string &str);
    private:
        iconv_t iconv_handle;
        cAuth m_auth;
        std::string m_page;
        std::string m_regex;
    };
}
#endif // CPAGEPARSER_H
