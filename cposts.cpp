#include "cposts.h"
namespace vk{
    cPosts::cPosts(const std::string &user, const std::string &password)
        : cPageParser(user, password){
        setRegex("<div class=\"wall_text_name\"><a class=\"author\" href=\"/([^\"]+?)\"[^>]*?>([^<]+?)</a></div>.*?"
                                "<div class=\"[wall_post_text|page_post_sized_thumbs][^>]+?>(.*?)</div>");
        setPage("vk.com/feed");
    }
    void cPosts::process(match_t &what){
        post_t post;
        post.author.id    = convert(what[1]);
        post.author.name  = escape_html(convert(what[2]));
        post.text         = escape_html(convert(what[3]));
        push_back(post);
    }
}
