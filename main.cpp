#include <iostream>
#include <fstream>
#include <memory>
#include <QApplication>
#include <QWidget>
#include <QtGui>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include "cposts.h"
#include "cuserinfo.h"
#include "cfriends.h"
#include "thread.h"
#include "cpoststhread.h"
#include "cuserinfothread.h"
#include "cfriendsthread.h"

#include "cuserinfowidget.h"
#include "cfriendswidget.h"
#include "cpostswidget.h"

// program requires boost_regex, libcurl
// to install it on ubuntu run:
// sudo apt-get install libcurl4-openssl-dev libboost-regex1.53-dev


int main(int argc, char **argv){
    QApplication a(argc, argv);
    QWidget w;
    w.setGeometry(0, 0, 1000, 500);

    QLabel loginLabel(&w);
    QLabel passwordLabel(&w);
    QLabel idLabel(&w);
    loginLabel.setGeometry(10, 310, 100, 30);
    passwordLabel.setGeometry(10, 370, 150, 30);
    idLabel.setGeometry(10, 430, 100, 30);
    loginLabel.setText("Enter login:");
    passwordLabel.setText("Enter password:");
    idLabel.setText("Enter ID:");

    QLineEdit usrLoginWidget(&w);
    QLineEdit usrPasswordWidget(&w);
    QLineEdit usrIdWidget(&w);
    usrLoginWidget.setGeometry(10, 340, 300, 30);
    usrPasswordWidget.setGeometry(10, 400, 300, 30);
    usrPasswordWidget.setEchoMode(QLineEdit::Password);
    usrIdWidget.setGeometry(10, 460, 300, 30);

    cPostsWidget postsWidget(&w, &usrLoginWidget, &usrPasswordWidget);
    postsWidget.setGeometry(10, 0, 300, 300);
    QPushButton btnPosts(&w);
    btnPosts.setGeometry(630, 370, 300, 30);
    btnPosts.setText("Show posts!");
    QObject::connect(&btnPosts, SIGNAL(clicked()), &postsWidget, SLOT(update()));

    cFriendsWidget friendsWidget(&w, &usrLoginWidget, &usrPasswordWidget);
    friendsWidget.setGeometry(320, 0, 300, 300);
    QPushButton btnFriends(&w);
    btnFriends.setGeometry(630, 340, 300, 30);
    btnFriends.setText("Show friends!");
    QObject::connect(&btnFriends, SIGNAL(clicked()), &friendsWidget, SLOT(update()));

    cUserInfoWidget userInfoWidget(&w, &usrIdWidget);
    userInfoWidget.setGeometry(630, 0, 300, 300);
    QPushButton btnInfo(&w);
    btnInfo.setGeometry(630, 400, 300, 30);
    btnInfo.setText("User Info");

    QObject::connect(&usrPasswordWidget, SIGNAL(returnPressed()), &postsWidget, SLOT(update()));
    QObject::connect(&usrPasswordWidget, SIGNAL(returnPressed()), &friendsWidget, SLOT(update()));
    QObject::connect(&usrIdWidget, SIGNAL(returnPressed()), &userInfoWidget, SLOT(update()));

    QObject::connect(&btnInfo, SIGNAL(clicked()), &userInfoWidget, SLOT(update()));

    w.show();
    return a.exec();
}

