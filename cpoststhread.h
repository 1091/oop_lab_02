#ifndef CPOSTSTHREAD_H
#define CPOSTSTHREAD_H

#include "cposts.h"
#include <mutex>
#include <QObject>
#include "thread.h"

namespace vk{
    class cPostsThread : public QObject, public cPosts{
        Q_OBJECT
    public:
        cPostsThread(const std::string &user, const std::string &password);
        void getThreadSafety();
        void getPosts();
    private:
        static std::mutex mGetMutex;
    signals:
        bool postsIsGet(bool);
    };

    class cFunctorPosts{
    public:
        cFunctorPosts(cPostsThread* postsThread);
        void operator () ();
    private:
       cPostsThread* postsThread;
    };
}

#endif // CPOSTSTHREAD_H
