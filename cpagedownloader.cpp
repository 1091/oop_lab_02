#include "cpagedownloader.h"
#include <iostream>
#include <sstream>
#include <curl/curl.h>

namespace vk{
    cPageDownloader::cPageDownloader(const std::string &page, const std::string &cookies){
        struct local{
            static size_t write_data(void* buffer, size_t size, size_t nmemb, void* userp)
            {
                std::ostream& stream = *reinterpret_cast<std::ostream*>(userp);
                stream.write(reinterpret_cast<char*>(buffer), size * nmemb);
                return size * nmemb;
            };
        };
        std::stringstream stream;
        CURL* curl = curl_easy_init();
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, local::write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA,     &stream);
        curl_easy_setopt(curl, CURLOPT_HEADER,        1L);
        if (cookies.length() > 0)
            curl_easy_setopt(curl, CURLOPT_COOKIE,        cookies.c_str());

        curl_easy_setopt(curl, CURLOPT_USERAGENT,     "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.66 Safari/537.36");
        curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
        curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1L);
        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        curl_easy_setopt(curl, CURLOPT_URL,     page.c_str());
        curl_easy_perform(curl);
        stream << '\0';

        curl_easy_cleanup(curl);
        m_page = stream.str();
    }
    const std::string &cPageDownloader::page() const {
        return m_page;
    }
}
