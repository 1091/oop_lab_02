#ifndef CFRIENDSTHREAD_H
#define CFRIENDSTHREAD_H

#include "cfriends.h"
#include "thread.h"
#include <mutex>
#include <QObject>


namespace vk{
    class cFriendsThread : public QObject, public cFriends{
        Q_OBJECT
    public:
        cFriendsThread(const std::string &login, const std::string &password);
        void getThreadSafety();
        void getFriends();
    private:
        static std::mutex mGetMutex;
    signals:
        bool friendsIsGet(bool);
    };

    class cFunctorFriends{
    public:
        cFunctorFriends(cFriendsThread* friendsThread);
        int operator () ();
    private:
        cFriendsThread* friendsThread;
    };
}

#endif // CFRIENDSTHREAD_H
