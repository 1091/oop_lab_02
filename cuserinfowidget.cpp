#include "cuserinfowidget.h"

cUserInfoWidget::cUserInfoWidget(QWidget *parent, QLineEdit* userIdWidget) :
    QTableWidget(parent){
    this->userIdWidget = userIdWidget;
    verticalHeader()->setVisible(false);
    horizontalHeader()->setVisible(false);
    verticalScrollBar()->setVisible(false);
    setShowGrid(false);
    setSelectionMode(QTableWidget::NoSelection);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    horizontalHeader()->setStretchLastSection(true);
}

void cUserInfoWidget::update(){
    userInfoThread = new vk::cUserInfoThread(userIdWidget->text().toStdString());
    connect(userInfoThread, SIGNAL(infoIsGet(bool)), this, SLOT(getNewData(bool)));
    userInfoThread->getUserInfo();
}

void cUserInfoWidget::getNewData(bool succes){
    if(succes){
        setRowCount(15);
        setColumnCount(2);
        int k = 0;
        for (auto it : (*userInfoThread)){
            QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(QString::fromUtf8(it.first.c_str())));
            setItem(k, 0, newItem);
            newItem = new QTableWidgetItem(tr("%1").arg(QString::fromUtf8(it.second.c_str())));
            setItem(k, 1, newItem);
            resizeRowsToContents();
            ++k;
        }
    } else {
        setRowCount(1);
        setColumnCount(1);
        clear();
        QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg("Wrong ID"));
        setItem(0, 0, newItem);
        resizeRowsToContents();
    }
}

