#include "cpoststhread.h"

namespace vk{
    cPostsThread::cPostsThread(const std::string &user, const std::string &password) : cPosts(user, password) {}
    std::mutex cPostsThread::mGetMutex;

    void cPostsThread::getThreadSafety(){
        mGetMutex.lock();
        bool success = get();
        mGetMutex.unlock();
        emit postsIsGet(success);
    }

    void cPostsThread::getPosts(){
        cFunctorPosts functor(this);
        Thread<cFunctorPosts> thread(functor);
        thread.start();
        thread.wait();
    }

    cFunctorPosts::cFunctorPosts(cPostsThread* postsThread){
        this->postsThread = postsThread;
    }

    void cFunctorPosts::operator () (){
       postsThread->getThreadSafety();
    }
}
