#include "cpageparser.h"
#include "cpagedownloader.h"

namespace  vk{
    cPageParser::cPageParser(){
        iconv_handle = iconv_open("utf-8", "windows-1251");
        if (iconv_handle == (iconv_t)-1)
            throw std::runtime_error("Failed to create instance of iconv");
    }
    cPageParser::cPageParser(const std::string& user, const std::string& password)
        : m_auth(user, password){
        iconv_handle = iconv_open("utf-8", "windows-1251");
        if (iconv_handle == (iconv_t)-1)
            throw std::runtime_error("Failed to create instance of iconv");
    }
    cPageParser::~cPageParser(){
        iconv_close(iconv_handle);
    }
    bool cPageParser::get(){
        if (m_page.empty() || m_regex.empty()){
            return false;
        }
        std::string feed = cPageDownloader(m_page, m_auth);
        if (feed.empty()){
            return false;
        }
        auto start = feed.begin();
        auto end = feed.end();

        boost::match_results<std::string::iterator> what;
        boost::match_flag_type flags = boost::match_default;

        boost::regex expression(m_regex);
        bool found = false;
        while(regex_search(start, end, what, expression, flags)){
            found = true;
            process(what);
            start = what[0].second;
            // update flags:
            flags |= boost::match_prev_avail;
            flags |= boost::match_not_bob;
        }
        return found;
    }
    void cPageParser::setRegex(const std::string &regex){
        m_regex = regex;
    }
    void cPageParser::setPage(const std::string &page){
        m_page = page;
    }
    std::string cPageParser::escape_html(const std::string &str){
        std::string s1, s2, s3;
        boost::regex regex_tags("<[^>]+?>");
        s1 = boost::regex_replace(str, regex_tags, s1);
    //    std::cout << str << " -> " << s1 << std::endl;
        boost::regex white_spaces("[\\s|\\n|\\t]{2,}");
        s2 = boost::regex_replace(s1, white_spaces, s2);
        boost::regex html_sym("&.+?;");
        s3 = boost::regex_replace(s1, html_sym, s3);

        return s3;
    }
    std::string cPageParser::convert(const std::string &source){
        size_t in_size = source.size();
        size_t out_size = 4 * in_size; // weird things happens while encoding
        char *in_ptr = const_cast<char*>(source.c_str());
        char *dest = new char [out_size];
        char *out_ptr = dest;
        memset(out_ptr, 0, out_size);
        size_t nconv = 0;
        while (in_size > 0){
            nconv = iconv(iconv_handle, &in_ptr, &in_size, &out_ptr, &out_size);
            if (nconv == (size_t)-1){
                std::cerr << "Error while converting string \"" << source  << "\"" << std::endl;
                break;
            }
        }
        return dest;
    }
}
