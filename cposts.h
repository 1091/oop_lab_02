#ifndef CPOSTS_H
#define CPOSTS_H
#include "vk.h"
#include "cpageparser.h"
#include <list>
#include <string>

namespace vk{
    class cPosts : public cPageParser, public std::list<post_t>{
    public:
        cPosts(const std::string& user, const std::string& password);
    protected:
        virtual void process(match_t &what);
    };
}
#endif // CPOSTS_H
