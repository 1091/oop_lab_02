#include "cpostswidget.h"

cPostsWidget::cPostsWidget(QWidget *parent, QLineEdit* loginWidget, QLineEdit* passwordWidget) :
    QTableWidget(parent){
    this->loginWidget = loginWidget;
    this->passwordWidget = passwordWidget;
    verticalHeader()->setVisible(false);
    horizontalHeader()->setVisible(false);
    verticalScrollBar()->setVisible(false);
    setShowGrid(false);
    setSelectionMode(QTableWidget::NoSelection);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    horizontalHeader()->setStretchLastSection(true);
}

void cPostsWidget::update(){
    postsThread = new vk::cPostsThread(loginWidget->text().toStdString(), passwordWidget->text().toStdString());
    connect(postsThread, SIGNAL(postsIsGet(bool)), this, SLOT(getNewData(bool)));
    postsThread->getPosts();
}

void cPostsWidget::getNewData(bool succes){
    if(succes){
        setRowCount(15);
        setColumnCount(2);
        int k = 0;
        for (auto it : (*postsThread)){
            QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(QString::fromUtf8(it.author.name.c_str()) ));
            setItem(k, 0, newItem);
            newItem = new QTableWidgetItem(tr("%1").arg(QString::fromUtf8(it.text.c_str())));
            setItem(k, 1, newItem);
            resizeRowsToContents();
            ++k;
        }
    } else {
        setRowCount(1);
        setColumnCount(1);
        clear();
        QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg("Wrong LOGIN or PASSWORD"));
        setItem(0, 0, newItem);
        resizeRowsToContents();
    }
}
