#ifndef CUSERINFOWIDGET_H
#define CUSERINFOWIDGET_H

#include <QWidget>
#include <QTableWidget>
#include <QHeaderView>
#include <QScrollBar>
#include <QObject>
#include <QLineEdit>
#include "cuserinfothread.h"
#include <QDebug>
class cUserInfoWidget : public QTableWidget{
    Q_OBJECT
public:
    explicit cUserInfoWidget(QWidget *parent = 0, QLineEdit* userIdWidget = 0);
private:
    vk::cUserInfoThread *userInfoThread;
    QLineEdit * userIdWidget;
public slots:
    void update();
    void getNewData(bool);
};

#endif // CUSERINFOWIDGET_H
