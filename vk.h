#ifndef VK_H
#define VK_H
#include <list>
#include <string>

namespace vk{
    struct author_t{
        std::string id;
        std::string name;
    };
    struct post_t{
        author_t    author;
        std::string text;
    };
    // pair -> field name, field value
    typedef std::pair<std::string, std::string> user_info_t;
}
#endif // VK_H
