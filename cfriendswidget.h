#ifndef CFRIENDSWIDGET_H
#define CFRIENDSWIDGET_H

#include <QWidget>
#include <QTableWidget>
#include <QHeaderView>
#include <QScrollBar>
#include <QObject>
#include <QLineEdit>
#include <QDebug>
#include "cfriendsthread.h"

class cFriendsWidget : public QTableWidget{
    Q_OBJECT
public:
    explicit cFriendsWidget(QWidget *parent = 0, QLineEdit* loginWidget = 0, QLineEdit* passwordWidget = 0);
private:
    vk::cFriendsThread *friendsThread;
    QLineEdit * loginWidget;
    QLineEdit * passwordWidget;
public slots:
    void update();
    void getNewData(bool);
};

#endif // CFRIENDSWIDGET_H
